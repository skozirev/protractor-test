const welcomePage = require('pages/WelcomePage');
const quickStartPage = require('pages/QuickStartPage');
const docsNavigationPage = require('pages/DocsNavigationPage');

describe('angular.io protractor test', function() {

	it('Search component and follow Getting started page', async function() {
		browser.get('/')

		welcomePage.typeSearch('component')
		welcomePage.checkSearchResultsPresent()
		welcomePage.typeSearch('')
		welcomePage.checkSearchResultsNotPresent()

		welcomePage.followGetStarted()
		quickStartPage.checkMainInfo()

		docsNavigationPage.hideNavigation()
		docsNavigationPage.checkNavigationNotExists()
	});
});